package com.beermon;

public enum BeermonConfig {
    KC("http://beermon.firemon.com/", 106), Dallas("http://beermon.firemon.com/dallas", 106);

    String beermonUrl;
    int scale;

    BeermonConfig(String url, int scale)
    {
        this.beermonUrl = url;
        this.scale = scale;
    }
}
